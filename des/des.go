package des

import (
	error2 "bitbucket.org/playsure-go/crypto/error"
	"bitbucket.org/playsure-go/crypto/operation"
	"bitbucket.org/playsure-go/crypto/operation/cbc"
	"bitbucket.org/playsure-go/crypto/operation/cfb"
	"bitbucket.org/playsure-go/crypto/operation/ecb"
	"bitbucket.org/playsure-go/crypto/padding"
	"crypto/des"
)

func Encrypt(srcData []byte, key []byte, iv []byte,
	operationMode operation.OperationMode, paddingMode padding.PaddingMode) ([]byte, error) {
	block, err := des.NewCipher(key)
	if err != nil {
		return nil, err
	}

	switch operationMode {
	case operation.OPERATION_MODE_CBC:
		return cbc.Encrypt(block, srcData, iv, paddingMode)
	case operation.OPERATION_MODE_ECB:
		return ecb.Encrypt(block, srcData, paddingMode)
	case operation.OPERATION_MODE_CFB:
		return cfb.Encrypt(block, srcData, iv)
	}
	return nil, error2.ErrorEncryptFailed
}

func Decrypt(srcData []byte, key []byte, iv []byte,
	operationMode operation.OperationMode, paddingMode padding.PaddingMode) ([]byte, error) {
	block, err := des.NewCipher(key)
	if err != nil {
		return nil, err
	}

	switch operationMode {
	case operation.OPERATION_MODE_CBC:
		return cbc.Decrypt(block, srcData, iv, paddingMode)
	case operation.OPERATION_MODE_ECB:
		return ecb.Decrypt(block, srcData, paddingMode)
	case operation.OPERATION_MODE_CFB:
		return cfb.Decrypt(block, srcData, iv)
	}
	return nil, error2.ErrorDecryptFailed
}
