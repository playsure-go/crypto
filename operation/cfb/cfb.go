package cfb

import "crypto/cipher"

func Encrypt(block cipher.Block, srcData []byte, iv []byte) ([]byte, error) {
	destData := make([]byte, len(srcData))
	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(destData, srcData)
	return destData, nil
}

func Decrypt(block cipher.Block, srcData []byte, iv []byte) ([]byte, error) {
	destData := make([]byte, len(srcData))
	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(destData, srcData)
	return destData, nil
}
