package ecb

import (
	"bitbucket.org/playsure-go/crypto/padding"
	"crypto/cipher"
)

func Encrypt(block cipher.Block, srcData []byte, paddingMode padding.PaddingMode) ([]byte, error) {
	blockSize := block.BlockSize()
	srcData = padding.Padding(srcData, blockSize, paddingMode)
	destData := make([]byte, len(srcData))
	tempDest := destData
	for len(srcData) > 0 {
		block.Encrypt(tempDest, srcData[:blockSize])
		srcData = srcData[blockSize:]
		tempDest = tempDest[blockSize:]
	}
	return destData, nil
}

func Decrypt(block cipher.Block, srcData []byte, paddingMode padding.PaddingMode) ([]byte, error) {
	blockSize := block.BlockSize()
	destData := make([]byte, len(srcData))
	tempDest := destData
	for len(srcData) > 0 {
		block.Decrypt(tempDest, srcData[:blockSize])
		srcData = srcData[blockSize:]
		tempDest = tempDest[blockSize:]
	}
	destData = padding.UnPadding(destData, paddingMode)
	return destData, nil
}
