package operation

type OperationMode string

const (
	OPERATION_MODE_CBC OperationMode = "CBC"
	OPERATION_MODE_ECB OperationMode = "ECB"
	OPERATION_MODE_CFB OperationMode = "CFB"
)
