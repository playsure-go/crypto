package cbc

import (
	"bitbucket.org/playsure-go/crypto/padding"
	"crypto/cipher"
)

func Encrypt(block cipher.Block, srcData []byte, iv []byte, paddingMode padding.PaddingMode) ([]byte, error) {
	blockSize := block.BlockSize()
	srcData = padding.Padding(srcData, blockSize, paddingMode)
	blockMode := cipher.NewCBCEncrypter(block, iv)
	destData := make([]byte, len(srcData))
	blockMode.CryptBlocks(destData, srcData)
	return destData, nil
}

func Decrypt(block cipher.Block, srcData []byte, iv []byte, paddingMode padding.PaddingMode) ([]byte, error) {
	blockMode := cipher.NewCBCDecrypter(block, iv)
	destData := make([]byte, len(srcData))
	blockMode.CryptBlocks(destData, srcData)
	destData = padding.UnPadding(destData, paddingMode)
	return destData, nil
}
