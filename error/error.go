package error

import "errors"

var (
	ErrorEncryptFailed = errors.New("encrypt failed")
	ErrorDecryptFailed = errors.New("decrypt failed")
)
