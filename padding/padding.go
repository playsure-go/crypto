package padding

import "bytes"

type PaddingMode string

const (
	PADDING_MODE_NONE  PaddingMode = "NONE"
	PADDING_MODE_PKCS7 PaddingMode = "PKCS7"
	PADDING_MODE_PKCS5 PaddingMode = "PKCS5"
	PADDING_MODE_ZEROS PaddingMode = "ZEROS"
)

func Padding(srcData []byte, blockSize int, padding PaddingMode) []byte {
	var destData []byte
	switch padding {
	case PADDING_MODE_PKCS7:
		destData = Pkcs7Padding(srcData, blockSize)
	case PADDING_MODE_PKCS5:
		destData = Pkcs5Padding(srcData, blockSize)
	case PADDING_MODE_ZEROS:
		destData = ZerosPadding(srcData, blockSize)
	case PADDING_MODE_NONE:
		destData = srcData
	}
	return destData
}

func UnPadding(srcData []byte, padding PaddingMode) []byte {
	var destData []byte
	switch padding {
	case PADDING_MODE_PKCS7:
		destData = Pkcs7UnPadding(srcData)
	case PADDING_MODE_PKCS5:
		destData = Pkcs5UnPadding(srcData)
	case PADDING_MODE_ZEROS:
		destData = ZerosUnPadding(srcData)
	case PADDING_MODE_NONE:
		destData = srcData
	}
	return destData
}

func Pkcs7Padding(srcData []byte, blockSize int) []byte {
	padding := blockSize - len(srcData)%blockSize
	paddingBytes := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(srcData, paddingBytes...)
}

func Pkcs7UnPadding(srcData []byte) []byte {
	length := len(srcData)
	unPaddingLength := int(srcData[length-1])
	return srcData[:length-unPaddingLength]
}

func Pkcs5Padding(srcData []byte, blockSize int) []byte {
	return Pkcs7Padding(srcData, blockSize)
}

func Pkcs5UnPadding(srcData []byte) []byte {
	return Pkcs7UnPadding(srcData)
}

func ZerosPadding(srcData []byte, blockSize int) []byte {
	padding := blockSize - len(srcData)%blockSize
	paddingBytes := bytes.Repeat([]byte{0}, padding)
	return append(srcData, paddingBytes...)
}

func ZerosUnPadding(srcData []byte) []byte {
	return bytes.TrimRightFunc(srcData,
		func(r rune) bool { return r == rune(0) })
}
